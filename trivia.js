var trivia = [



    // 0.

    function () {
        var one = 1;
        var One = 1;

        return one === One;
    },

    // A. true
    // B. false
    // C. (error)
    // D. (none of the above)



    // 1.

    function () {
        var obj = {};
        var obj = {};

        return obj === obj;
    },

    // A. true
    // B. false
    // C. (error)
    // D. (none of the above)



    // 2.

    function () {
        var array = []
        [1, 2].forEach(function (value) {
            array.push(value);
        });
        return array;
    },

    // A. []
    // B. [1, 2]
    // C. (error)
    // D. (none of the above)



    // 3.

    function () {
        var obj = {};
        var Obj = {};

        return obj === Obj;
    },

    // A. true
    // B. false
    // C. (error)
    // D. (none of the above)



    // 4.

    function () {
        return null == undefined && undefined == null;
    },

    // A. true
    // B. false
    // C. (error)
    // D. (none of the above)



    // 5.

    function () {
        return 0.1 + 0.2;
    },

    // A. 3
    // B. 0.3
    // C. (error)
    // D. (none of the above)



    // 6.

    function () {
        return Infinity - 1;
    },

    // A. 1.7976931348623157e+308
    // B. Infinity
    // C. 0
    // D. -1



    // 7.

    function () {
        return 'hello'.toUpperCase();
    },

    // A. 'HELLO'
    // B. (error)
    // C. -1
    // D. 'hello'



    // 8.

    function () {
        return NaN == NaN;
    },

    // A. true
    // B. false
    // C. -1
    // D. NaN



    // 9.

    function () {
        var string = 'hello';

        string.toUpperCase();

        return string;
    },

    // A. 'hello'
    // B. 'HELLO'
    // C. undefined
    // D. (none of the above)



    // 10.

    function () {
        var a = 1;
        var b = 2;

        var add = function (a, b) {
            return a + b;
        };

        return add();
    },

    // A. undefined
    // B. NaN
    // C. 3
    // D. (error)



    // 11.

    function () {
        var user = { firstName: 'Bobby', lastName: 'Clams' };

        return user.firstName === user['firstName'] === user[firstName];
    },

    // A. true
    // B. false
    // C. (error)
    // D. (none of the above)



    // 12.

    function () {
        var data = { one: 1, two: 2 };

        return !data.two + 2 * 5;
    },

    // A. false
    // B. 20
    // C. 0
    // D. 10



    // 13.

    function () {
        var a = 1;
        var b = 1;

        if (a < 1)
            a = 1;
            b = 2;

        if (a === 1)
            a = 2;

        return a + b;
    },

    // A. 4
    // B. 3
    // C. 2
    // D. (error)



    // 14.

    function () {
        return typeof Object.create(null);
    },

    // A. 'object'
    // B. 'null'
    // C. undefined
    // D. (error)



    // 15.

    function () {
        var video = { title: 'Grape Lady Falls', category: 'comedy' };

        return video.runTime.length;
    },

    // A. -1
    // B. 0
    // C. undefined
    // D. (error)



    // 16.

    function () {
        var array = [ , , , ];

        return array.length;
    },

    // A. 0
    // B. 3
    // C. 4
    // D. (error)



    // 17.

    function () {
        var array = [1, 2, 3];

        array.length = 1;

        return array[2];
    },

    // A. undefined
    // B. 3
    // C. -1
    // D. (error)



    // 18.

    function () {
        var array = [33, 4, 1111, 222];

        return array.sort();
    },

    // A. 1111, 222, 33, 4
    // B. 4, 33, 222, 1111
    // C. 33, 4, 1111, 222
    // D. (none of the above)



    // 19.

    function () {
        var array = [1, 2, 3];

        return array.forEach(function (value) {
            value += 1;
        });
    },

    // A. [1, 2, 3]
    // B. [2, 3, 4]
    // C. undefined
    // D. (none of the above)



    // 20.

    function () {
        return

        1 + 1;
    },

    // A. 2
    // B. "2"
    // C. undefined
    // D. (none of the above)



    // 21.

    function () {
        var a = [function(x) { return x * x; }, 2];

        return a[0](a[1]);
    },

    // A. 4
    // B. 2
    // C. undefined
    // D. (error)



    // 22.

    function () {
        var a = 1;
        var b = '2';
        var add = function () { return a + b; };
        return typeof add;
    },

    // A. 'number'
    // B. 'string'
    // C. 'function'
    // D. ''



    // 23.

    function () {
        var getValue = function () {
            'use strict';
            return this;
        };
        var mogwai = { name: 'Gizmo', getValue: getValue };
        var gremlin = { name: 'Mohawk' };
        window.name = 'Billy';

        return mogwai.getValue().name;
    },

    // A. 'Billy'
    // B. 'Gizmo'
    // C. 'Mohawk'
    // D. (error)



    // 24.

    function () {
        var getValue = function () {
            'use strict';
            return this;
        };
        var mogwai = { name: 'Gizmo', getValue: getValue };
        var gremlin = { name: 'Mohawk' };
        window.name = 'Billy';

        return gremlin.getValue().name;
    },

    // A. 'Billy'
    // B. 'Gizmo'
    // C. 'Mohawk'
    // D. (error)



    // 25.

    function () {
        var getValue = function () {
            'use strict';
            return this;
        };
        var mogwai = { name: 'Gizmo', getValue: getValue };
        var gremlin = { name: 'Mohawk' };
        window.name = 'Billy';

        return mogwai.getValue.call(gremlin).name;
    },

    // A. 'Billy'
    // B. 'Gizmo'
    // C. 'Mohawk'
    // D. (error)



    // 26.

    function () {
        var getValue = function () {
            'use strict';
            return this;
        };
        var mogwai = { name: 'Gizmo', getValue: getValue };
        var gremlin = { name: 'Mohawk' };
        window.name = 'Billy';

        return getValue().name;
    },

    // A. 'Billy'
    // B. 'Gizmo'
    // C. 'Mohawk'
    // D. (error)



    // 27.

    function () {
        var getValue = function () {
            return this;
        };
        var mogwai = { name: 'Gizmo', getValue: getValue };
        var gremlin = { name: 'Mohawk' };
        window.name = 'Billy';

        return getValue().name;
    },

    // A. 'Billy'
    // B. 'Gizmo'
    // C. 'Mohawk'
    // D. (error)



];
